# 1. Повертати вміст файлу з пайтон пакетами (requirements.txt)
# PATH: /requirements/ відкрити файл requirements.txt і повернути його вміст
# 2. Вивести 100 випадково згенерованих користувачів (пошта + ім'я) 'John aasdasda@mail.com'
# PATH: /generate-users/ ( https://pypi.org/project/Faker/ )
# + параметр який регулює кількість користувачів
# 3. Вивести кількість космонавтів в даний момент ((http://api.open-notify.org/astros.json)
# (https://pypi.org/project/requests/)
# PATH: /space/

from flask import Flask, request
from utils import user_info
import requests

app = Flask(__name__)


@app.route("/requirements")
def requirements():
    with open("requirements.txt", "r") as file:
        content = file.read()
    return content


@app.route("/generate-users")
def generate_users():
    number_str = request.args.get('number', '100')

    if not number_str.isdigit():
        return 'Number should be a digit'

    number = int(number_str)

    if number <= 0:
        return 'Number should be positive digit'

    if number > 1000:
        return 'Number is too long'

    return user_info(number)


@app.route("/space")
def space():

    response = requests.get('http://api.open-notify.org/astros.json')
    data = response.json()['number']
    result = f'The number of astronauts in orbit: {data}'
    return result


if __name__ == '__main__':
    app.run()
