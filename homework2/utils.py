from faker import Faker
fake = Faker()


def user_info(number: int) -> str:
    users = ''
    for _ in range(number):
        user = f'{fake.name()} {fake.email()}\n'
        users += user
    return users
