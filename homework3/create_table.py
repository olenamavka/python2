import sqlite3
con = sqlite3.connect("tutorial.db")
cur = con.cursor()


sql = '''
CREATE TABLE Phones (
    PhoneID INTEGER PRIMARY KEY,
    ContactName varchar(255),
    PhoneValue varchar(255)
);
'''
cur.execute(sql)
con.commit()
con.close()
