from faker import Faker
fake = Faker()


def user_info(number: int) -> str:
    users = ''
    for _ in range(number):
        user = f'{fake.name()} {fake.email()}\n'
        users += user
    return users


def execute_sql(sql: str) -> None:
    import sqlite3
    con = sqlite3.connect("tutorial.db")
    cur = con.cursor()
    cur.execute(sql)
    con.commit()
    con.close()
