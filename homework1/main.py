def parse(query: str) -> dict:
    dct_query = {}
    if '?' in set(query):
        params = query.split("?")[-1]
        query_params = list(filter(None, params.split('&')))
        for pair in query_params:
            parameters = pair.split('=')
            dct_query.update({parameters[0]: parameters[1]})
    return dct_query


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=John') == {'name': 'John'}


def parse_cookie(query: str) -> dict:
    dct_query = {}
    params = list(filter(None, query.split(';')))
    for pair in params:
        parameters = pair.split('=', 1)
        dct_query.update({parameters[0]: parameters[1]})
    return dct_query


if __name__ == '__main__':
    assert parse_cookie('name=John;') == {'name': 'John'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=John;age=28;') == {'name': 'John', 'age': '28'}
    assert parse_cookie('name=John=User;age=28;') == {'name': 'John=User', 'age': '28'}
